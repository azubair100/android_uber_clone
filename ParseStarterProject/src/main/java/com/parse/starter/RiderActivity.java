package com.parse.starter;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;


//START CODING FROM onMapReady()

public class RiderActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    LocationManager locationManager;

    LocationListener locationListener;

    Button callUberButton;

    //SET THIS UP WHEN THE USER REQUESTS A NEW UBER
    //requestActive = true; SEARCH THAT
    Handler handler= new Handler();

    TextView infoTextView;

    Boolean driverActive = false;

    //THIS METHOD DOES NUMBER OF THINGS
    //IF SOME ONE HAS PICKED UP THE REQUEST OR NOT, PRESENCE OF A DRIVER USERNAME
    //SO GET THE REQUEST OBJECT
    public void checkForUpdates(){

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");

        //GET THE CURRENT RIDER'S REQUESTS
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());

        //WHERE THERE IS A DRIVER USERNAME
        query.whereExists("driverUsername");

        query.findInBackground(
                new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {

                        if(e == null && objects.size() > 0){
                            driverActive = true;

                            //FIND THE DRIVER USERNAME TO GET DRIVER LOCATION
                            ParseQuery<ParseUser> query1 = ParseUser.getQuery();
                            query1.whereEqualTo("username", objects.get(0).getString("driverUsername"));

                            query1.findInBackground(
                                    new FindCallback<ParseUser>() {
                                        @Override
                                        public void done(List<ParseUser> objects, ParseException e) {

                                            if(e == null && objects.size() > 0){

                                                //THE FIRST SECTION MEASURES HOW MNAY MILES AWAY THE DRIVER IS

                                                //GET DRIVER'S LOCATION HERE
                                                ParseGeoPoint driverLocation = objects.get(0).getParseGeoPoint("location");

                                                //BUT WE ALSO NEED TO COMPARE THE USER'S LOCATION AND DRIVER'S LOCATION SO WE NEED THE USER'S MOST CURRENT LOCATION
                                                if (ContextCompat.checkSelfPermission(RiderActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                                                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                                                    Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                                                    if (lastKnownLocation != null) {

                                                        //USER LOCATION IN GEOPOINT
                                                        ParseGeoPoint userLocation = new ParseGeoPoint(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());

                                                        //NOW WE COMPARE THE DRIVER LOCATION AND USER LOCATION TO GET THE DISTANCE
                                                        //OVER IN VIEWREQUST ACTIVITY
                                                        Double distanceInMiles =
                                                                (double) Math.round((driverLocation.distanceInMilesTo(userLocation))*10)/10;

                                                        //UPDATE THE APP IF THE DRIVER IS AT THE LOCATION OF THE RIDER
                                                        if(distanceInMiles  < 0.01){

                                                            infoTextView.setText("Your driver is here");


                                                            //NOW WE DELETE THE REQUEST
                                                            ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");

                                                            //GET THE CURRENT RIDER'S REQUESTS
                                                            query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());

                                                           query.findInBackground(
                                                                   new FindCallback<ParseObject>() {
                                                                       @Override
                                                                       public void done(List<ParseObject> objects, ParseException e) {

                                                                           if( e== null){
                                                                               for(ParseObject object: objects){
                                                                                   object.deleteInBackground();
                                                                               }
                                                                           }

                                                                       }
                                                                   }
                                                           );




                                                            //SET UP A NEW RUNNABLE TO UPDATE THE USER
                                                            handler.postDelayed(
                                                                    //THIS IS A METHOD WE CAN RUN EVERY 5 SECONDS MINUTES ETC
                                                                    new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            //THIS CODE WILL BE CALLED FROM NUMBER OF PLACES SO MADE IT INTO A METHOD

                                                                            callUberButton.setVisibility(View.VISIBLE);
                                                                            callUberButton.setText("Call an Uber");
                                                                            requestActive = false;
                                                                            driverActive = false;

                                                                        }
                                                                    },
                                                                    5000
                                                            );


                                                        }
                                                        else{

                                                            //THAT MEANS WE HAVE A DRIVER ON OUR WAY
                                                            //SET UP THE INFO BUTTON HERE
                                                            infoTextView.setText("Your driver is " + distanceInMiles + " miles away!");


                                                            //THIS IS THE SECTION WHERE IT SHOWS THE RIDER WHERE THE DRIVER IS
                                                            //WHICH IS IN DRIVER LOCATIONS ACTIVITY


                                                            //CREATE A LAT LANG BOUNDS OBJECT, DOING THAT FROM AN ARRAY LIST OF MARKERS
                                                            ArrayList<Marker> markers = new ArrayList<>();

                                                            LatLng driverLocationLatLng = new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude());

                                                            LatLng requestLocationLatLng = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());


                                                            //THE PART WHERE IT SHOWS ON THE MAP WHERE THE RIDER IS AND WHERE THE DRIVER IS
                                                            markers.add(mMap.addMarker(new MarkerOptions().position(driverLocationLatLng).title("Driver location")));
                                                            markers.add(mMap.addMarker(new MarkerOptions().position(requestLocationLatLng).title("Your location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))));


                                                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                                            for (Marker marker : markers) {
                                                                builder.include(marker.getPosition());
                                                            }
                                                            LatLngBounds bounds = builder.build();
                                                            //Then obtain a movement description object by using the factory: CameraUpdateFactory:

                                                            int padding = 60; // offset from edges of the map in pixels
                                                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                                            //Finally move the map:

                                                            //googleMap.moveCamera(cu);
                                                            //Or if you want an animation:

                                                            mMap.animateCamera(cu);

                                                            callUberButton.setVisibility(View.INVISIBLE);

                                                            handler.postDelayed(
                                                                    //THIS IS A METHOD WE CAN RUN EVERY 5 SECONDS MINUTES ETC
                                                                    new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            //THIS CODE WILL BE CALLED FROM NUMBER OF PLACES SO MADE IT INTO A METHOD
                                                                            checkForUpdates();

                                                                        }
                                                                    },
                                                                    2000
                                                            );

                                                        }

                                                        //NOW YOU CHECK WHEN THE USERS LOCATION UPDATES WE DON'T WANT TO UPDATE THE MAP
                                                        //ONLY UPDATE THE MAP USING THE UPDATE METHOD RATHER THAN CENTRING IT ON THE USER

                                                    }


                                                }



                                                }

                                        }
                                    }
                            );





                        }


                    }
                }
        );



    }

    //APP STATE
    Boolean requestActive = false;

    public void logOut(View view) {

        ParseUser.logOut();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);

    }

    //THIS IS A METHOD FOR SAVING A PARSE OBJECT, REQUEST
    public void callUber(View view) {

        Log.i("Info", "Call Uber");

        //CANCEL SECTION
        //THE REQUEST IS NOW ACTIVE, SO ENTER THIS BLOCK TO CANCEL IT
        //WHICH WILL DELETE THE REQUEST FROM THE PARSE DATABASE

        if (requestActive) {

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Request");

            query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());

            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {

                    if (e == null) {

                        if (objects.size() > 0) {

                            for (ParseObject object : objects) {

                                //THIS IS HOW YOU DELETE A PARSE OBJECT
                                object.deleteInBackground();

                            }

                            //THERE ARE NO REQUESTS SO SET IT TO FALSE AND CHANGE TEXT
                            requestActive = false;
                            callUberButton.setText("Call An Uber");

                        }

                    }

                }
            });


        } else {

            //YOU NEED TO RE VERIFY THE PERMISSION FOR ACCESSING THE MAPS
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                //IF LAST KNOWN LOCATION IS AVAILABLE YOU CAN CREATE THE REQUEST ROW, SINCE LAST KNOWN LOCATION IS NECESSARY
                if (lastKnownLocation != null) {

                    //WE ARE CREATING THE NEW TABLE
                    ParseObject request = new ParseObject("Request");

                    //FILLING THE FIELDS
                    //USERNAME
                    request.put("username", ParseUser.getCurrentUser().getUsername());

                    //ParseGeoPoint IS A BUILT IN DATA TYPE FOR LOCATION, WILL TAKE LONG AND LAT AUTOMATICALLY
                    ParseGeoPoint parseGeoPoint = new ParseGeoPoint(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());

                    request.put("location", parseGeoPoint);

                    //SAVE THE REQUEST
                    request.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e == null) {

                                //REQUEST SAVED, SO CHANGE THE BUTTON TEXT AND THE STATE VARIABLE TO TRUE
                                callUberButton.setText("Cancel Uber");
                                requestActive = true;

                                //WHEN WE COME TO THE ACTIVITY BUT THERE IS ALREADY A REQUEST ACTIVE
                                checkForUpdates();

                            }

                        }
                    });

                }
                //NO LOCATION FOUND, SO CAN NOT CREATE THE REQUEST AND CAN NOT CALL THE UBER
                else {

                    Toast.makeText(this, "Could not find location. Please try again later.", Toast.LENGTH_SHORT).show();

                }

            }

        }


    }

    //THIS METHOD COMES TO PLAY WHEN THE USER HAS GIVEN US PERMISSION
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //ALMOST 99.9% SURE IT WILL BE 1 ALL THE TIME
        if (requestCode == 1) {

            //WE HAVE A RESULT AND IT IS PERMISSION GRANTED
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //EVEN THOUGH WE CHECKED IF PERMISSION WAS GRANTED, BUT WE NEED TO DO AN EXPLICIT CHECK
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    //CHECK THE MANIFEST, ACCESS FINE LOCATION
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                    Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    updateMap(lastKnownLocation);


                }

            }


        }

    }

    //THIS METHOD TAKES THE LOCATION AND PUTS IT ON THE MAP, IT JUST UPDATES THE MAP WHEN A LOCATION IS GIVEN
    //if (Build.VERSION.SDK_INT < 23) {
    public void updateMap(Location location) {

        if(driverActive != false){

            LatLng userLocation = new LatLng(location.getLatitude(), location.getLongitude());

            mMap.clear();
            // ZOOMING INTO THE USER LOCATION A BIT MORE
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15));
            mMap.addMarker(new MarkerOptions().position(userLocation).title("Your Location"));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        callUberButton = (Button) findViewById(R.id.callUberButton);

        infoTextView = (TextView) findViewById(R.id.infoTextView);

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Request");

        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                if (e == null) {

                    if (objects.size() > 0) {

                        requestActive = true;
                        callUberButton.setText("Cancel Uber");

                    }

                }

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //DECLARE LOCATION MANAGER AND LOCATION LISTENER
        //SET THEM UP INSIDE ON MAP READY()
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            //THIS IS THE METHOD WE ARE INTERESTED IN
            @Override
            public void onLocationChanged(Location location) {

                //AFTER YOU SET UP THE MARKERS ON THE MAP, SEEK PERMISSION, HERE
                updateMap(location);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        //IF LESS THAN 23 ASK DIRECTLY
        if (Build.VERSION.SDK_INT < 23) {

            //RECEIVE THE UPDATES ON THE MAP, ANYTIME THE USER MOVES IN ANY GIVEN TIME
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        }
        //AFTER 23 MUST ASK FOR PERMISSION
        else {

            //PERMISSION HAS NOT BEEN GRANTED YET
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


            }

            //PERMISSION HAS ALREADY BEEN GRANTED
            else {

                //RECIEVE THE UPDATES ON THE MAP, ANYTIME THE USER MOVES IN ANY GIVEN TIME
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                // ***** MOST RECENT LOCATION *****
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (lastKnownLocation != null) {

                    updateMap(lastKnownLocation);
                    //AFTER THIS CREATE THE onRequestPermissionsResult
                    //BECAUSE THE USER HAS GIVEN US PERMISSION

                }


            }


        }

    }
}
