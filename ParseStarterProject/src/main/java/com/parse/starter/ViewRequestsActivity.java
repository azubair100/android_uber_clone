package com.parse.starter;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ViewRequestsActivity extends AppCompatActivity {

    ListView requestListView;

    LocationManager locationManager;

    LocationListener locationListener;
    
    ArrayList<String> requests = new ArrayList<>();
    
    ArrayAdapter arrayAdapter;

    ArrayList<Double> requestLatitude = new ArrayList<>();

    ArrayList<Double> requestLongitude = new ArrayList<>();

    ArrayList<String> usernames = new ArrayList<>();


    //INSTEAD OF THE UPDATEMAP FROM RIDER'S ACTIVITY YOU HAVE THIS MEHTOD
    //THIS METHOD WILL GET ALL OF THE NEWBY LOCATIONS AND UPDATE THE MAP
    public void updateListView(Location location){

        if(location != null){

            //YOU SET UP A PARSE OBJECT QUERY TO GET ALL ACTIVE REQUESTS
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");

            //BUT THE DRIVER ALSO NEEDS IT'S CURRENT LOCATION TO MEASURE THE NEARBY LOCATIONS
            final ParseGeoPoint geoPointLocation = new ParseGeoPoint(location.getLatitude(), location.getLongitude());

            //THIS WILL FIND THE OBJECTS THAT ARE NEAR OUR LOCATION, AND WILL ORDER THEM CLOSEST
            query.whereNear("location", geoPointLocation);

            //WHERE THE DRIVER USERNAME IS UNDEFINED
            query.whereDoesNotExist("driverUsername");

            query.setLimit(10);

            query.findInBackground(
                    new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {

                            if(e == null){

                                //YOU CLEAR ALL OLD REQUESTS
                                requests.clear();
                                requestLatitude.clear();
                                requestLongitude.clear();

                                if(objects.size()>0){

                                    for(ParseObject object: objects){

                                        //WE WANT TO SHOW THE DISTANCE OF THE REQUEST FROM THE DRIVER'S CURRENT LOCATOIN
                                        ParseGeoPoint requestLocation = (ParseGeoPoint) object.get("location");
                                        if(requestLocation != null){

                                            //WE PUT REQUEST LOCATIONS INSIDE THE distanceInMilesTo
                                            //GET THE DISTANCE IN MILES
                                            Double distanceInMiles =
                                                    (double) Math.round((geoPointLocation.distanceInMilesTo(requestLocation))*10)/10;

                                            //ADD TO THE ARRAY LIST
                                            requests.add(distanceInMiles.toString() + " miles");

                                            //LONGITUDE AND LATITUDE FILL
                                            requestLatitude.add(requestLocation.getLatitude());
                                            requestLongitude.add(requestLocation.getLongitude());

                                            //USERNAMES FILL
                                            usernames.add(object.getString("username"));

                                        }
                                    }

                                }

                                //no near by requests
                                else{
                                    requests.add("No active requests nearby");
                                }

                                //NOW UPDATE THE LISTVIEW
                                arrayAdapter.notifyDataSetChanged();
                            }

                        }
                    }
            );

        }


    }


    //THIS METHOD COMES TO PLAY WHEN THE USER HAS GIVEN US PERMISSION FOR GETTING THEIR LOCATION
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //ALMOST 99.9% SURE IT WILL BE 1 ALL THE TIME
        if (requestCode == 1) {

            //WE HAVE A RESULT AND IT IS PERMISSION GRANTED
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //EVEN THOUGH WE CHECKED IF PERMISSION WAS GRANTED, BUT WE NEED TO DO AN EXPLICIT CHECK
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    //CHECK THE MANIFEST, ACCESS FINE LOCATION
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                    Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    updateListView(lastKnownLocation);


                }

            }


        }

    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_requests);

        setTitle("Nearby Requests");

        //LIST VIEW FOR VIEWING ALL THE REQUESTS
        requestListView = (ListView) findViewById(R.id.requestListView);


        //SET UP THE ADAPTER
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, requests);

        //GET RID OFF OLD REQUESTS
        requests.clear();


        requests.add("Getting near by requests ... ");


        requestListView.setAdapter(arrayAdapter);

        //YOU HAVE TO SEND THE DRIVER'S LOCATION AND THE REQUEST LOCATION TO THE DRIVER LOCATION MAP ACTIVITY
        //MAKE THE ITEMS IN THE LIST VIEW CLICKABLE

        //WE CAN NOT SEND LOCATIONS DIRECTLY THROUGH INTENTS, THEREFORE DECLARE ArrayList<Float> requestLatitude

        requestListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

Log.i("grant status", ContextCompat.checkSelfPermission(ViewRequestsActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) + "");
Log.i("build version status", PackageManager.PERMISSION_GRANTED + "");

                        //CHECK IF WE CAN GET THE LOCATIONS WE NEED TO SEND
                        //
                        //IF PERMISSION WAS GRANTED
                        if (ContextCompat.checkSelfPermission(ViewRequestsActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                )
                        {
                            Log.i("clicked", "ON ITEM " + position);

                            //YOU NEED THE MOST RECENT LOCATION OF THE DRIVER TO COMPARE THE DISTANCE FROM THE OTHER REQUESTS
                            // ***** MOST RECENT LOCATION *****
                            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                            //ALL THE ARRAY LISTS MUST HAVE A HIGHER SIZE THAN THE POSITION THAT WAS CLICKED
                            if (requestLatitude.size() > position &&
                                    requestLongitude.size() > position &&
                                    usernames.size() > position &&
                                    lastKnownLocation != null) {

                                //HAVE TO SEND THE DRIVER'S LOCATION AND THE REQUEST LOCATION ON THE MAP ACTIVITY FOR THE DRIVER
                                Intent intent = new Intent(ViewRequestsActivity.this, DriverLocationActivity.class);
                                intent.putExtra("requestLatitude", requestLatitude.get(position));
                                intent.putExtra("requestLongitude", requestLongitude.get(position));
                                intent.putExtra("driverLatitude", lastKnownLocation.getLatitude());
                                intent.putExtra("driverLongitude", lastKnownLocation.getLongitude());

                                //YOU WILL NEED THE USERNAME AS WELL, SO CREATE AN ARRAYLIST OF USERS UP TOP
                                intent.putExtra("username", usernames.get(position));



                                startActivity(intent);
//                                startActivityForResult(intent, 0);
//                                finish();

                            }
                        }

                    }
                }
        );




        //DECLARE LOCATION MANAGER AND LOCATION LISTENER
        //SET THEM UP INSIDE ON MAP READY()
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            //THIS IS THE METHOD WE ARE INTERESTED IN
            //change driver's location whenever it changes or updates
            @Override
            public void onLocationChanged(Location location) {

                //AFTER YOU SET UP THE MARKERS ON THE MAP, SEEK PERMISSION, HERE
                updateListView(location);

                //SAVE IT TO PARSE SERVER SO THE FEATURE
                //RIDER CAN SEE WHERE THERE UBER DRIVER IS

                ParseUser.getCurrentUser().put("location", new ParseGeoPoint(location.getLatitude(), location.getLongitude()));
                ParseUser.getCurrentUser().saveInBackground();

                //NOW THE DRIVER LOCATION HAS BEEN UPDATED
                //NOW REPORT IT BACK TO THE RIDER IN RIDER'S ACTIVITY
                // TO DO THAT WE RUN A PEICE OF CODE EVERY FEW SECONDS TO UPDATE THE DRIVER'S LOCATION ON THE RIDER'S MAP
                //HANDLER IMPLEMENTED IN RIDER ACITIVITY


            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        //IF LESS THAN 23 REQUEST LOCATION UPDATE DIRECTLY
        if (Build.VERSION.SDK_INT < 23) {

            //RECEIVE THE UPDATES ON THE MAP, ANYTIME THE USER MOVES IN ANY GIVEN TIME
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        }
        //AFTER 23 MUST ASK FOR PERMISSION
        else {

            //PERMISSION HAS NOT BEEN GRANTED YET
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);


            }

            //PERMISSION HAS ALREADY BEEN GRANTED
            else {

                //RECIEVE THE UPDATES ON THE MAP, ANYTIME THE USER MOVES IN ANY GIVEN TIME
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                // ***** MOST RECENT LOCATION *****
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (lastKnownLocation != null) {

                    updateListView(lastKnownLocation);
                    //AFTER THIS CREATE THE onRequestPermissionsResult
                    //BECAUSE THE USER HAS GIVEN US PERMISSION

                }


            }


        }

    }
    
}
