/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class MainActivity extends AppCompatActivity {

  //IF THE USER IS A RIDER, REDIRECT TO RIDERS ACTIVITY MAP
  public void redirectActivity() {

    if (ParseUser.getCurrentUser().getString("riderOrDriver").equals("rider")) {

        Intent intent = new Intent(getApplicationContext(), RiderActivity.class);
        startActivity(intent);

    }

    else{

      Intent intent = new Intent(getApplicationContext(), ViewRequestsActivity.class);
      startActivity(intent);
    }


  }

  //THIS IS THE BUTTON TO GET STARTED, SWITCH
  public void getStarted(View view) {

    Switch userTypeSwitch = (Switch) findViewById(R.id.userTypeSwitch);

    Log.i("Switch value", String.valueOf(userTypeSwitch.isChecked()));

    //DEFAULT CHOICE IS RIDER
    String userType = "rider";

    //IF USER SAYS DRIVER CHANGE IT
    if (userTypeSwitch.isChecked()) {

      userType = "driver";

    }

    //SAVE THE USER OR UPDATE THE USER
    ParseUser.getCurrentUser().put("riderOrDriver", userType);

      ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {

            //IF IT WAS SAVED AS rider THEN REDIRECT TO RIDER ACTIVITY
              redirectActivity();

          }
      });




  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getSupportActionBar().hide();


//IF THE CURRENT USER IS NOT LOGGED IN, LOG THEM IN USING PARSE ANONYMOUS OBJECT
    if (ParseUser.getCurrentUser() == null) {

      ParseAnonymousUtils.logIn(new LogInCallback() {
        @Override
        public void done(ParseUser user, ParseException e) {

          if (e == null) {

            Log.i("Info", "Anonymous login successful");

          } else {

            Log.i("Info", "Anonymous login failed");

          }


        }
      });

    }
    //WE DO NOT WANT TO KEEP LOGGING THE USER IN, IF THEY ARE ALREADY LOGGED IN
    else {

      //IF THE CURRENT USER TYPE IS NOT NULL, AND IF THE TYPE IS rider, THEN GO TO THE MAPS ACTIVITY
      if (ParseUser.getCurrentUser().get("riderOrDriver") != null) {

        Log.i("Info", "Redirecting as " + ParseUser.getCurrentUser().get("riderOrDriver"));

        redirectActivity();

      }


    }


    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }

}