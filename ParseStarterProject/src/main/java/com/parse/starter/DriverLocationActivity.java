package com.parse.starter;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class DriverLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_location);

        //RECEIVE THE REQUEST WHICH CONTAINS DRIVER LOCATION AND THE RIDER LOCATOIN
        intent = getIntent();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //YOU NEED TO HAVE ACCESS TO THE RELATIVE LAYOUT, OTHERWISE THE MAP MIGHT NOT LOAD.
        //THIS CODE HELPS THE MAP LOAD
        RelativeLayout mapLayout = (RelativeLayout)findViewById(R.id.mapRelativeLayout);
        mapLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {


                //CREATE A LAT LANG BOUNDS OBJECT, DOING THAT FROM AN ARRAY LIST OF MARKERS
                ArrayList<Marker> markers = new ArrayList<>();

                LatLng driverLocation = new LatLng(intent.getDoubleExtra("driverLatitude", 0), intent.getDoubleExtra("driverLongitude", 0));

                LatLng requestLocation = new LatLng(intent.getDoubleExtra("requestLatitude", 0), intent.getDoubleExtra("requestLongitude", 0));


                //THE PART WHERE IT SHOWS ON THE MAP WHERE THE RIDER IS AND WHERE THE DRIVER IS
                markers.add(mMap.addMarker(new MarkerOptions().position(driverLocation).title("Marker in Driver location")));
                markers.add(mMap.addMarker(new MarkerOptions().position(requestLocation).title("Marker in Request location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))));


                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (Marker marker : markers) {
                    builder.include(marker.getPosition());
                }
                LatLngBounds bounds = builder.build();
                //Then obtain a movement description object by using the factory: CameraUpdateFactory:

                int padding = 60; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                //Finally move the map:

                //googleMap.moveCamera(cu);
                //Or if you want an animation:

                mMap.animateCamera(cu);

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng driverLocation = new LatLng(intent.getDoubleExtra("driverLatitude", 0), intent.getDoubleExtra("driverLongitude", 0));
        //mMap.addMarker(new MarkerOptions().position(driverLocation).title("Marker in Driver location"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(driverLocation));
    }

    //UPDATE THE REQUEST OVER IN PARSE, SO WE KNOW IT HAS BEEN ACCEPTED
    //AND GIVE THE DRIVER DIRECTIONS
    public void acceptRequest(View view) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");

        query.whereEqualTo("username", intent.getStringExtra("username"));

        query.findInBackground(

                new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {


                        if(e==null){

                            if(objects.size() > 0){

                                for(ParseObject object: objects){

                                    object.put("driverUsername", ParseUser.getCurrentUser().getUsername());

                                    object.saveInBackground(
                                            new SaveCallback() {
                                                @Override
                                                public void done(ParseException e) {

                                                    if(e== null){

                                                        //GET DIRECTIONS TO THE NEW LOCATION OPEN GOOGLE MAPS
        Intent directionsIntent = new Intent(android.content.Intent.ACTION_VIEW,
        Uri.parse("http://maps.google.com/maps?saddr= " + intent.getDoubleExtra("driverLatitude", 0) + "," + intent.getDoubleExtra("driverLongitude", 0) +"&daddr=" + intent.getDoubleExtra("requestLatitude", 0) + "," + intent.getDoubleExtra("requestLongitude", 0)));
         startActivity(directionsIntent);

                                                    }

                                                }
                                            }
                                    );

                                }

                            }

                        }

                    }
                }
        );

    }
}
